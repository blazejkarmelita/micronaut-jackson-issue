#!/bin/bash

ENVIRONMENT_TYPE=$1
ENVIRONMENT_ALIAS=$2

setup_template() {
  SCRIPT_PATH="./deployment/scripts"
  CLOUDFORMATION_TEMPLATE="$SCRIPT_PATH/cloudformation-template.sh"
  CLOUDFORMATION_FILE="cloudformation.yaml"
  MODULE_NAME="sample-module"
}

run_template() {
  setup_template

  # shellcheck disable=SC1090
  source $CLOUDFORMATION_TEMPLATE "$CLOUDFORMATION_FILE" "$MODULE_NAME" "$ENVIRONMENT_TYPE" "$ENVIRONMENT_ALIAS" "$SCRIPT_PATH"
}

# main executable
run_template



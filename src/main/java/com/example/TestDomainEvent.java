package com.example;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(as = ImmutableTestDomainEvent.class)
@JsonTypeName("easmith.test.event.TestDomainEvent")
public interface TestDomainEvent {

    @Value.Default
    default String eventType() {
        return "easmith.test.event.TestDomainEvent";
    };

}

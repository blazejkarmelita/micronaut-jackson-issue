package com.example;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import javax.inject.Singleton;
import java.io.IOException;

@Singleton
public class CustomBookDeserializer extends JsonDeserializer<Book> {

    public CustomBookDeserializer() {
        super();
    }

    @Override
    public Book deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return Book.ofName("Mabe not Serverless");
    }

}

package com.example;

import edu.umd.cs.findbugs.annotations.NonNull;
import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;

//@Introspected
public class Book {

    @NonNull
    @NotBlank
    private String name;

//    public Book() {
//    }

    private Book(String name) {
        super();

        this.name = name;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public static Book ofName(String name) {
        return new Book(name);
    }
}

package com.example;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class BookRequestHandlerTest {

    private static BookRequestHandler bookRequestHandler;

    @BeforeAll
    public static void setupServer() {
        bookRequestHandler = new BookRequestHandler();
    }

    @AfterAll
    public static void stopServer() {
        if (bookRequestHandler != null) {
            bookRequestHandler.getApplicationContext().close();
        }
    }

    @Test
    public void testHandler() throws JsonProcessingException {
        final String json = "{ name: \"An old book\"}";
        final ObjectMapper objectMapper = bookRequestHandler.getApplicationContext().getBean(ObjectMapper.class);
        final Book book = objectMapper.readValue(json, Book.class);

        BookSaved bookSaved = bookRequestHandler.execute(book);
        assertEquals(bookSaved.getName(), book.getName());
        assertNotNull(bookSaved.getIsbn());
    }
}

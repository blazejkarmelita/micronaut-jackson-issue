#!/bin/bash

print_help() {
  echo -e "Usage: $ ./deploy-stack.sh [bucket_name] [stack_name] [template] [deployment_parameters]"
}

S3_BUCKET_NAME=$1
CLOUDFORMATION_STACK_NAME=$2
CLOUDFORMATION_TEMPLATE_FILE=$3
DEPLOYMENT_PARAMS=$4

if [ -z "${S3_BUCKET_NAME}" ]; then
  echo "S3 bucket name parameter not provided."
  print_help
  exit 1
fi

if [ -z "${CLOUDFORMATION_STACK_NAME}" ]; then
  echo "Cloudformation stack name parameter not provided."
  print_help
  exit 1
fi

if [ -z "${CLOUDFORMATION_TEMPLATE_FILE}" ]; then
  echo "Cloudformation template file name parameter not provided. Using 'cloudformation.yaml' as default."
  CLOUDFORMATION_TEMPLATE_FILE="cloudformation.yaml"
fi

if [ -z "${DEPLOYMENT_PARAMS}" ]; then
  echo "Deployment parameters not provided."
  DEPLOYMENT_PARAMS=""
fi

if [ "$1" = 'help' ]
  then
    print_help
    exit 1
fi

# packaging logic
package_deployment() {
  echo "Packaging deployment for cloudformation template: $CLOUDFORMATION_TEMPLATE_FILE ..."

  rm -rf .aws-sam
  sam build -t ./${CLOUDFORMATION_TEMPLATE_FILE}
  echo "Deployment package ready."
  echo ""

  deploy;
}

# deployment logic
deploy() {
  echo -e "Deploying cloudformation stack: $CLOUDFORMATION_STACK_NAME from s3://$S3_BUCKET_NAME ..."
  echo -e "Using the following deployment parameters: $DEPLOYMENT_PARAMS"

  sam deploy \
     --template-file .aws-sam/build/template.yaml \
     --stack-name ${CLOUDFORMATION_STACK_NAME} \
     --capabilities CAPABILITY_IAM \
     --parameter-overrides ${DEPLOYMENT_PARAMS} \
     --s3-bucket ${S3_BUCKET_NAME} \
     --region ${AWS_REGION}

  echo "Done."
  echo ""
}

# main executable
package_deployment
#!/bin/bash

print_help() {
  echo -e "Usage: $ ./cloudformation.sh [yaml_file] [module_name] [environment_type] [environment_alias] [script_path]"
}

CLOUDFORMATION_FILE=$1
MODULE_NAME=$2
ENVIRONMENT_TYPE=$3
ENVIRONMENT_ALIAS=$4
SCRIPT_PATH=$5

if [ -z "${CLOUDFORMATION_FILE}" ]; then
  echo "Cloudformation file parameter not provided."
  print_help
  exit 1
fi

if [ -z "${MODULE_NAME}" ]; then
  echo "Module name parameter not provided."
  print_help
  exit 1
fi

if [ -z "${ENVIRONMENT_TYPE}" ]; then
  echo -e "Environment type parameter not provided."
  print_help
  exit 1
fi

if [ -z "${ENVIRONMENT_ALIAS}" ]; then
  echo -e "Environment alias parameter not provided."
  print_help
  exit 1
fi

if [ -z "${SCRIPT_PATH}" ]; then
  echo -e "Script path parameter not provided. Using '.' as default."
  SCRIPT_PATH="."
fi

if [ "$1" = 'help' ]
  then
    print_help
    exit 1
fi

setup_scripts() {
  LOAD_CONFIGURATION_SCRIPT="$SCRIPT_PATH/load-configuration.sh"
  CREATE_S3_BUCKET_SCRIPT="$SCRIPT_PATH/create-s3-bucket.sh"
  DEPLOY_STACK_SCRIPT="$SCRIPT_PATH/deploy-stack.sh"
}

setup_deployment() {
  # shellcheck disable=SC1090
  source $LOAD_CONFIGURATION_SCRIPT $ENVIRONMENT_TYPE $ENVIRONMENT_ALIAS

  CLOUDFORMATION_STACK_NAME=${ENVIRONMENT_ALIAS}-${ENVIRONMENT_TYPE}-${MODULE_NAME}
  DEPLOYMENT_PARAMS="EnvironmentType=${ENVIRONMENT_TYPE} DeployAccount=${AWS_ACCOUNT} CloudformationStackName=${CLOUDFORMATION_STACK_NAME}"
  MVN_PROFILE=${ENVIRONMENT_TYPE}
}

run_deployment() {
  # shellcheck disable=SC1090
  source $CREATE_S3_BUCKET_SCRIPT $S3_CODE_BUCKET

  # shellcheck disable=SC1090
  source $DEPLOY_STACK_SCRIPT $S3_CODE_BUCKET $CLOUDFORMATION_STACK_NAME $CLOUDFORMATION_FILE "$DEPLOYMENT_PARAMS"
}

process_cloudformation() {
  #validate_input
  setup_scripts
  setup_deployment
  run_deployment
}

# main executable
process_cloudformation
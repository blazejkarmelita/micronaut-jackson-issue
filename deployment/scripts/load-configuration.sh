#!/bin/bash

print_help() {
  echo -e "Usage: $ ./load-configuration.sh [environment_type] [environment_alias]"
}

ENVIRONMENT_TYPE=$1
ENVIRONMENT_ALIAS=$2

if [ -z "${ENVIRONMENT_TYPE}" ]; then
  echo "Environment type parameter not provided."
  print_help
  exit 1
fi

if [ -z "${ENVIRONMENT_ALIAS}" ]; then
  echo "Environment alias parameter not provided."
  print_help
  exit 1
fi

if [ "$1" = 'help' ]
  then
    print_help
    exit 1
fi

# configuration logic
load_configuration() {
  echo "Preparing deployment configuration for environment: $ENVIRONMENT_TYPE $ENVIRONMENT_ALIAS ..."

  configure_aws
  configure_s3_code_bucket

  echo -e "Configuration ready.\n"
}

configure_aws() {
  AWS_ACCOUNT="645798406675"
  AWS_REGION="eu-west-1"
  echo -e "Using AWS account: $AWS_ACCOUNT with region: $AWS_REGION."
}

configure_s3_code_bucket() {
  S3_CODE_BUCKET="${ENVIRONMENT_ALIAS}-${ENVIRONMENT_TYPE}-deploy-bucket"
  echo -e "S3 code bucket configured to: $S3_CODE_BUCKET"
}

# main executable
load_configuration
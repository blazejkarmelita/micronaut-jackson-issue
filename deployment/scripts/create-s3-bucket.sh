#!/bin/bash

print_help() {
  echo -e "Usage: $ ./create-s3-bucket.sh [bucket_name]"
}

S3_BUCKET_NAME=$1

if [ -z "${S3_BUCKET_NAME}" ]; then
  echo "S3 bucket name parameter not provided."
  print_help
  exit 1
fi

if [ "$1" = 'help' ]
  then
    print_help
    exit 1
fi

prepare_s3_bucket() {
  echo "Looking for S3 bucket: $S3_BUCKET_NAME ..."
  if aws s3 ls "s3://$S3_BUCKET_NAME" 2>&1 | grep -q 'NoSuchBucket'
  then
    echo "S3 Bucket: $S3_BUCKET_NAME not found. Creating new one ..."
    aws s3 mb s3://${S3_BUCKET_NAME}
    echo "S3 bucket: $S3_BUCKET_NAME created."
  else
    echo "S3 bucket: $S3_BUCKET_NAME ready."
  fi
  echo ""
}

# main executable
prepare_s3_bucket


